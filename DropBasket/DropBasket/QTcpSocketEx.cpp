#include "QTcpSocketEx.h"


void QTcpSocketEx::_readyRead() {
	emit readyReadEx(sock);
}

QTcpSocketEx::QTcpSocketEx(QTcpSocket* sock) : Socket(sock){
	this->sock = sock;
	connect(sock, SIGNAL(readyRead()), this, SLOT(_readyRead()));
}
/********************************************************************************
** Form generated from reading UI file 'dropbasket.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DROPBASKET_H
#define UI_DROPBASKET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DropBasketClass
{
public:
    QWidget *centralWidget;
    QPushButton *fStartServ;
    QLineEdit *fHostAddrToConnect;
    QLabel *label;
    QLabel *label_2;
    QSpinBox *fHostPortToConnect;
    QPushButton *fJoinToServ;
    QTextEdit *fConsole;
    QLabel *label_3;
    QLabel *label_4;
    QLineEdit *fClientMsg;
    QPushButton *fClientSend;
    QLineEdit *fServMsg;
    QPushButton *fServSend;
    QLineEdit *fServRootPath;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_8;
    QLineEdit *fClientRootPath;
    QLabel *label_9;
    QLabel *label_10;
    QLineEdit *fHostAddr;
    QLabel *label_11;
    QLabel *label_12;
    QSpinBox *fHostPort;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *DropBasketClass)
    {
        if (DropBasketClass->objectName().isEmpty())
            DropBasketClass->setObjectName(QStringLiteral("DropBasketClass"));
        DropBasketClass->resize(525, 540);
        centralWidget = new QWidget(DropBasketClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        fStartServ = new QPushButton(centralWidget);
        fStartServ->setObjectName(QStringLiteral("fStartServ"));
        fStartServ->setGeometry(QRect(30, 150, 211, 31));
        fHostAddrToConnect = new QLineEdit(centralWidget);
        fHostAddrToConnect->setObjectName(QStringLiteral("fHostAddrToConnect"));
        fHostAddrToConnect->setGeometry(QRect(290, 120, 113, 20));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(290, 100, 47, 13));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(420, 100, 47, 13));
        fHostPortToConnect = new QSpinBox(centralWidget);
        fHostPortToConnect->setObjectName(QStringLiteral("fHostPortToConnect"));
        fHostPortToConnect->setGeometry(QRect(420, 120, 81, 22));
        fHostPortToConnect->setMinimum(1111);
        fHostPortToConnect->setMaximum(9999);
        fHostPortToConnect->setValue(9898);
        fJoinToServ = new QPushButton(centralWidget);
        fJoinToServ->setObjectName(QStringLiteral("fJoinToServ"));
        fJoinToServ->setGeometry(QRect(290, 150, 211, 31));
        fConsole = new QTextEdit(centralWidget);
        fConsole->setObjectName(QStringLiteral("fConsole"));
        fConsole->setGeometry(QRect(30, 330, 471, 151));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(30, 10, 121, 21));
        QFont font;
        font.setPointSize(16);
        label_3->setFont(font);
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(290, 10, 131, 21));
        label_4->setFont(font);
        fClientMsg = new QLineEdit(centralWidget);
        fClientMsg->setObjectName(QStringLiteral("fClientMsg"));
        fClientMsg->setEnabled(false);
        fClientMsg->setGeometry(QRect(290, 220, 211, 20));
        fClientSend = new QPushButton(centralWidget);
        fClientSend->setObjectName(QStringLiteral("fClientSend"));
        fClientSend->setEnabled(false);
        fClientSend->setGeometry(QRect(290, 250, 211, 31));
        fServMsg = new QLineEdit(centralWidget);
        fServMsg->setObjectName(QStringLiteral("fServMsg"));
        fServMsg->setEnabled(false);
        fServMsg->setGeometry(QRect(30, 220, 211, 20));
        fServSend = new QPushButton(centralWidget);
        fServSend->setObjectName(QStringLiteral("fServSend"));
        fServSend->setEnabled(false);
        fServSend->setGeometry(QRect(30, 250, 211, 31));
        fServRootPath = new QLineEdit(centralWidget);
        fServRootPath->setObjectName(QStringLiteral("fServRootPath"));
        fServRootPath->setGeometry(QRect(30, 70, 211, 20));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(30, 50, 47, 13));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(30, 200, 81, 16));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(290, 50, 47, 13));
        fClientRootPath = new QLineEdit(centralWidget);
        fClientRootPath->setObjectName(QStringLiteral("fClientRootPath"));
        fClientRootPath->setGeometry(QRect(290, 70, 211, 20));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(290, 200, 71, 16));
        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(30, 310, 211, 16));
        fHostAddr = new QLineEdit(centralWidget);
        fHostAddr->setObjectName(QStringLiteral("fHostAddr"));
        fHostAddr->setGeometry(QRect(30, 120, 113, 20));
        fHostAddr->setReadOnly(true);
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(160, 100, 47, 13));
        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(30, 100, 47, 13));
        fHostPort = new QSpinBox(centralWidget);
        fHostPort->setObjectName(QStringLiteral("fHostPort"));
        fHostPort->setGeometry(QRect(160, 120, 81, 22));
        fHostPort->setMinimum(1111);
        fHostPort->setMaximum(9999);
        fHostPort->setValue(9898);
        DropBasketClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(DropBasketClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 525, 21));
        DropBasketClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(DropBasketClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        DropBasketClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(DropBasketClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        DropBasketClass->setStatusBar(statusBar);

        retranslateUi(DropBasketClass);

        QMetaObject::connectSlotsByName(DropBasketClass);
    } // setupUi

    void retranslateUi(QMainWindow *DropBasketClass)
    {
        DropBasketClass->setWindowTitle(QApplication::translate("DropBasketClass", "DropBasket", 0));
        fStartServ->setText(QApplication::translate("DropBasketClass", "Start server and run monitor", 0));
        fHostAddrToConnect->setText(QApplication::translate("DropBasketClass", "localhost", 0));
        label->setText(QApplication::translate("DropBasketClass", "Host address", 0));
        label_2->setText(QApplication::translate("DropBasketClass", "Port", 0));
        fJoinToServ->setText(QApplication::translate("DropBasketClass", "Connect to server", 0));
        label_3->setText(QApplication::translate("DropBasketClass", "// SERVER", 0));
        label_4->setText(QApplication::translate("DropBasketClass", "// CLIENT", 0));
        fClientSend->setText(QApplication::translate("DropBasketClass", "Send message to server", 0));
        fServSend->setText(QApplication::translate("DropBasketClass", "Send message to all clients", 0));
        fServRootPath->setText(QApplication::translate("DropBasketClass", "C:\\Users\\F_SNCHZ\\Documents\\DBDIR1", 0));
        label_5->setText(QApplication::translate("DropBasketClass", "Root path", 0));
        label_6->setText(QApplication::translate("DropBasketClass", "Text message", 0));
        label_8->setText(QApplication::translate("DropBasketClass", "Root path", 0));
        fClientRootPath->setText(QApplication::translate("DropBasketClass", "C:\\Users\\F_SNCHZ\\Documents\\DBDIR2", 0));
        label_9->setText(QApplication::translate("DropBasketClass", "Text message", 0));
        label_10->setText(QApplication::translate("DropBasketClass", "Logger", 0));
        fHostAddr->setText(QApplication::translate("DropBasketClass", "localhost", 0));
        label_11->setText(QApplication::translate("DropBasketClass", "Port", 0));
        label_12->setText(QApplication::translate("DropBasketClass", "Host address", 0));
    } // retranslateUi

};

namespace Ui {
    class DropBasketClass: public Ui_DropBasketClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DROPBASKET_H

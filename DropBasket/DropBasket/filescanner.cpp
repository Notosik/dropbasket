#include "filescanner.h"

QStringList _allFilesList;
QStringList _allDirsAndSubdirsList;

void _scanDir(const char* path)
{
	QDir dir(path);
	QString prefix(path); prefix += "\\";

	// dir.setNameFilters(QStringList("*.nut"));
	dir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);

	qDebug() << "Scanning: " << dir.path();

	QStringList fileList = dir.entryList();

	for (int i = 0; i < fileList.size(); i++)
		fileList[i] = prefix + fileList[i];
	_allFilesList.append(fileList);

	dir.setFilter(QDir::AllDirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);

	QStringList dirList = dir.entryList();
	for (int i = 0; i < dirList.size(); i++)
		dirList[i] = prefix + dirList[i];
	_allDirsAndSubdirsList.append(dirList);

	for (int i = 0; i<dirList.size(); i++)
	{
		_scanDir(dirList[i].toStdString().c_str());
	}
}

void getAllStuffFromDir(const char* path)
{
	_allFilesList.clear();
	_allDirsAndSubdirsList.clear();
	_scanDir(path);
}
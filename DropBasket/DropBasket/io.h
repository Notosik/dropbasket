#pragma once

#include <fstream>
#include <stdio.h>
#include <iostream>
#include <conio.h>
#include <windows.h>
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>
#include <vector>
#include <string>
using namespace std;

void rmFile(const char* path);
void mkFile(const char* path);
int deleteDirectory(const wstring &refcstrRootDirectory, bool bDeleteSubdirectories = true);
void createDir(const wchar_t* path);
#ifndef DROPBASKET_H
#define DROPBASKET_H

#include "TcpServ.h"

//#define UNICODE
#include <QtWidgets/QMainWindow>
#include "ui_dropbasket.h"
#include <QWidget>
#include <QtNetwork>
#include <sstream>
#include "filescanner.h"
#include "QTcpSocketEx.h"
#include "io.h"
#include "TcpServ.h"
#include <QMessageBox>
#include <QCloseEvent>

typedef unsigned char Byte;

class DropBasket : public QMainWindow
{
	Q_OBJECT

private:
	void closeEvent(QCloseEvent *event);

public:
	void getPathState();
	void checkPathDiff(QStringList &newFilesAndDirs, QStringList &deletedFilesAndDirs);
	void print(const char* text);
	void print(QString text);
	void print(const char* text, int value);
	void sync_step1(QStringList &newFilesAndDirs, QStringList &deletedFilesAndDirs);
	void sync_step2(QStringList &newFilesAndDirs, QStringList &deletedFilesAndDirs, SOCKET concreteClient = NULL, wchar_t* messageID = L"s");
	void sendBinary(SOCKET client, const char* fullFilePath);
	void sendAllBinaryFiles(SOCKET client);

	DropBasket(QWidget *parent = 0);
	~DropBasket();

	public slots:
	void startClient();
	void readFromServ();
	void startServer();
	void clientSend();
	void servSend();
	void synchronize();
	void pathChangeDetect(const QString& dir);
	void fileChangeDetect(const QString& dir);

private:
	Ui::DropBasketClass ui;
};

char* encodeLong(long value);
void setDB(DropBasket *dbwin);
DropBasket* getDB();

extern DropBasket* dbwin;

#endif // DROPBASKET_H

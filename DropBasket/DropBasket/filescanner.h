#include <windows.h>
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>
#include <vector>
#include <string>
#include <QtWidgets/QMainWindow>
#include <QWidget>
#include <QtNetwork>
#pragma comment(lib, "User32.lib")

using namespace std;

extern QStringList _allFilesList;
extern QStringList _allDirsAndSubdirsList;

void getAllStuffFromDir(const char* path);
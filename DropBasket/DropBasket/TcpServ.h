#pragma once

#include <vector>

#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <QtWidgets/QMainWindow>
#include "ui_dropbasket.h"
#include <QWidget>
#include <QtNetwork>
#include <sstream>
#include "filescanner.h"
#include "QTcpSocketEx.h"
#include "io.h"
#include "dropbasket.h"

#define DEFAULT_BUFLEN 5*1024*1024

extern bool stop_all;

extern bool servIsWaitingForRcvConfirmation;
extern vector<SOCKET> TCPSERV_clients;
extern void (*TCPSERV_readFromClient)(SOCKET, char*, int);
extern void(*TCPSERV_newClient)(SOCKET);

#pragma comment(lib, "Ws2_32.lib")

void sendToClient(SOCKET client, char* data, long datacount);
void startServer(const char* addrPort);
void destroy_all();

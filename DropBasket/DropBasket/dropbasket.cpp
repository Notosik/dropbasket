#include "dropbasket.h"

QTcpSocket *clientSock;


QStringList oldDirsState;
QStringList currDirsState;
QStringList oldFilesState;
QStringList currFilesState;


QString servRootPath;
QString clientRootPath;
QFileSystemWatcher *fileWatcher;

DropBasket *_db;

void setDB(DropBasket *dbwin) {
	_db = dbwin;
}

DropBasket* getDB() {
	return _db;
}

int WStringToString(string &s, wstring &ws)
{
	QString qs; qs = qs.fromStdWString(ws);
	s = qs.toStdString();

	return 0;
}

int StringToWString(wstring &ws, const string &s)
{
	QString qs; qs = qs.fromStdString(s);
	ws = qs.toStdWString();

	return 0;
}

string getSubPathOfFile(string fullFilePath, string rootPath) {
	int rootPathLen = rootPath.size();
	string subPath = fullFilePath.substr(rootPathLen + 1);
	return subPath;
}

char *readBinary(const char* filename, long &out_size) {
	std::ifstream infile(filename, std::ifstream::binary);

	// get size of file
	infile.seekg(0, infile.end);
	long size = infile.tellg();
	infile.seekg(0);

	out_size = size;

	// allocate memory for file content
	char* buffer = new char[size];

	// read content of infile
	infile.read(buffer, size);

	buffer[size] = '\0';

	infile.close();

	return buffer;
}

void writeBinary(const char* filename, char *content, long size) {
	std::ofstream outfile(filename, std::ofstream::binary);

	// write to outfile
	outfile.write(content, size);

	outfile.close();
}

QString getIPaddress() {
	foreach(const QHostAddress &address, QNetworkInterface::allAddresses()) {
		if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost))
			return address.toString();
	}
	return "";
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
	stringstream ss(s);
	string item;
	while (getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
	vector<string> elems;
	split(s, delim, elems);
	return elems;
}

void DropBasket::getPathState() {
	getAllStuffFromDir(servRootPath.toStdString().c_str());
	currDirsState = QStringList(_allDirsAndSubdirsList);
	currFilesState = QStringList(_allFilesList);
}

void DropBasket::checkPathDiff(QStringList &newFilesAndDirs, QStringList &deletedFilesAndDirs) {
	bool found; string t;

	for (int i = 0; i < oldDirsState.size(); i++) {
		found = false;
		for (int j = 0; j < currDirsState.size(); j++) {
			if (oldDirsState[i] == currDirsState[j]) {
				found = true;
				break;
			}
		}
		if (!found) {
			t = "d" + getSubPathOfFile(oldDirsState[i].toStdString(), servRootPath.toStdString());
			deletedFilesAndDirs.append(t.c_str());
		}
	}

	for (int i = 0; i < currDirsState.size(); i++) {
		found = false;
		for (int j = 0; j < oldDirsState.size(); j++) {
			if (currDirsState[i] == oldDirsState[j]) {
				found = true;
				break;
			}
		}
		if (!found) {
			t = "d" + getSubPathOfFile(currDirsState[i].toStdString(), servRootPath.toStdString());
			newFilesAndDirs.append(t.c_str());
		}
	}



	for (int i = 0; i < oldFilesState.size(); i++) {
		found = false;
		for (int j = 0; j < currFilesState.size(); j++) {
			if (oldFilesState[i] == currFilesState[j]) {
				found = true;
				break;
			}
		}
		if (!found) {
			t = "f" + getSubPathOfFile(oldFilesState[i].toStdString(), servRootPath.toStdString());
			deletedFilesAndDirs.append(t.c_str());
		}
	}


	for (int i = 0; i < currFilesState.size(); i++) {
		found = false;
		for (int j = 0; j < oldFilesState.size(); j++) {
			if (currFilesState[i] == oldFilesState[j]) {
				found = true;
				break;
			}
		}
		if (!found) {
			t = "f" + getSubPathOfFile(currFilesState[i].toStdString(), servRootPath.toStdString());
			newFilesAndDirs.append(t.c_str());
		}
	}
}

void DropBasket::print(QString text) {
	print(text.toStdString().c_str());
}

void DropBasket::print(const char* text) {
	ui.fConsole->append(text);
}

void DropBasket::print(const char* text, int value) {
	QString merge = QString::number(value);
	merge = text + merge;
	print(merge);
}

void DropBasket::startClient() {


	clientSock = new QTcpSocket(this);
	clientSock->setReadBufferSize(9999999);
	connect(clientSock, &QTcpSocket::readyRead, this, &DropBasket::readFromServ);
	
	clientSock->connectToHost(ui.fHostAddrToConnect->text(), (quint16)ui.fHostPortToConnect->value());

	clientRootPath = ui.fClientRootPath->text();

	ui.fJoinToServ->setEnabled(false);
	ui.fHostAddrToConnect->setEnabled(false);
	ui.fHostPortToConnect->setEnabled(false);
	ui.fClientRootPath->setEnabled(false);
	// ...
	ui.fStartServ->setEnabled(false);
	ui.fHostAddr->setEnabled(false);
	ui.fHostPort->setEnabled(false);
	ui.fServRootPath->setEnabled(false);
	// ...
	//ui.fClientMsg->setEnabled(true);
	ui.fClientSend->setEnabled(true);
}

long decodeLong(char* codedValue) {

	long value = 0;

	// this 4 is sizeof(long)
	for (int i = 0; i < 4; i++) {
		getDB()->print("long encode: ", (unsigned char)codedValue[i]);
		value += ((unsigned char)codedValue[i]) * pow(256, i);
	}

	return value;
}

void sendConfirmationToServ() {
	clientSock->write("r");
}

bool incomingMsgProcessing = false;
long fullMsgSize;
char* clientInBuff; long clientInBuffPos;

void DropBasket::readFromServ() {

	print("readFromServ");

	// read data from socket (from server)
	long bytesRead = clientSock->bytesAvailable();
	QByteArray arr = clientSock->readAll();
	char* data = arr.data();

	if (!incomingMsgProcessing) {

		long msgSize = decodeLong(data) - 4;
		bytesRead -= 4;
		data += 4;

		if (bytesRead >= msgSize) {
			print("bytes available: ", bytesRead);
			print("Serv want to say me something:");
			clientInBuff = data;
			fullMsgSize = msgSize;
			incomingMsgProcessing = false;
			// w tym miejscu sko�czylismy czytac wiadomosc a wiec mozemy wyslac serwerowi potwierdzenie odbioru
			sendConfirmationToServ();
		}else{
			print("read first portion of data");

			clientInBuff = new char[msgSize];
			memcpy(clientInBuff, data, bytesRead);
			clientInBuffPos = bytesRead;
			fullMsgSize = msgSize;
			incomingMsgProcessing = true;
			return;
		}


	}else{


		if (bytesRead + clientInBuffPos >= fullMsgSize) {
			print("bytes available: ", bytesRead);
			print("Serv want to say me something:");
			memcpy(&clientInBuff[clientInBuffPos], data, bytesRead);
			incomingMsgProcessing = false;
			// w tym miejscu sko�czylismy czytac wiadomosc a wiec mozemy wyslac serwerowi potwierdzenie odbioru
			sendConfirmationToServ();
		}
		else{
			memcpy(&clientInBuff[clientInBuffPos], data, bytesRead);
			clientInBuffPos += bytesRead;
			incomingMsgProcessing = true;
			return;
		}


	}

	data = clientInBuff;
	//data[fullMsgSize] = '\0';

	//convert this data to QString type
	//QString str = data;

	//convert QString to std::string
	//string rcv = str.toStdString();

	//print(rcv.c_str());

	// Je�li messageID = i to mamy do czynienia z init-sync (startowa synchronizacja nowego klienta)
	// w tym wypadku usuwamy wszystkie wczesniejsze pliki z root-path
	// dalej nast�puje zwyk�y proces synchronizacji (zamiana literki i na s) czyli "wklejamy" pliki i foldery przes�ane z serwera
	if (data[0] == 'i') {

		// usuwamy wszystkie pliki w root-path - najlatwiej to zrobic usuwajac po prostu root-path i tworz�c go na nowo
		print("Init sync. Start process of cleaning root-path");
		deleteDirectory(clientRootPath.toStdWString());
		Sleep(1000);
		createDir(clientRootPath.toStdWString().c_str());

		// Dalej zwyk�a synchronizacja
		data[0] = 's';
	}

	// Pierwszy przes�any znak wskazuje na TYP ZAWARTO�CI odebranej wiadomo�ci
	// m - text message
	// s - synchronize data
	// b - binary file content

	if (data[0] == 'm') {

		print("Odebrano komunikat typu M");

		print("Text message from server:");

		data[fullMsgSize] = '\0';

		print(data);

	}
	else if (data[0] == 'b') {

		print("Odebrano komunikat typu B");

		data += 1;
		// ...
		char *fileSubPathStrLen_coded = data;
		long fileSubPathStrLen = decodeLong(fileSubPathStrLen_coded);
		print("d�ugo�� nazwy pliku wynosi: ", fileSubPathStrLen);
		// ...
		data += 4;
		// ...
		char *fileSubPath = new char[fileSubPathStrLen+1];
		memcpy(fileSubPath, data, fileSubPathStrLen);
		fileSubPath[fileSubPathStrLen] = '\0';
		// ...
		data += fileSubPathStrLen;
		// ...
		char *filesize_coded = data;
		long filesize = decodeLong(filesize_coded);
		// ...
		data += 4;
		// ...
		char *content = data;
		content[filesize] = '\0';

		QString fullPath = clientRootPath; fullPath += "\\"; fullPath += fileSubPath;

		print("Full binary file path:");
		print(fullPath);

		print("Binary file content:");
		print(content);

		////mkFile(fullPath.toStdString().c_str());
		//Sleep(1000);
		writeBinary(fullPath.toStdString().c_str(), content, filesize);

	}
	else if (data[0] == 's') {

		print("Odebrano komunikat typu S");

		data[fullMsgSize] = '\0';
		data++;

		//split received data by ':' char (deletedFiles and newFIles)
		vector<string> x = split(data, ':');

		string deletedFilesStr = "";
		string newFilesStr = "";

		// decode received data
		if (x.size() == 0) {
			// no any changes
			return;
		}
		else if (x.size() == 1){
			int colonPos = strcmp(data, ":");
			if (colonPos > 1) {
				deletedFilesStr = x[0];
			}
			else{
				newFilesStr = x[0];
			}
		}
		else{ // size == 2
			newFilesStr = x[0];
			deletedFilesStr = x[1];
		}

		//get vector of deleted and new files and show result in console

		if (deletedFilesStr != "") {
			vector<string> deletedFiles = split(deletedFilesStr, '\n');
			print("Usunieto pliki/foldery:");
			for (int i = 0; i < deletedFiles.size(); i++) {
				print(deletedFiles[i].substr(1).c_str());

				if (deletedFiles[i][0] == 'f') {
					QString fullPath = clientRootPath; fullPath += "\\"; fullPath += deletedFiles[i].substr(1).c_str();
					rmFile(fullPath.toStdString().c_str());
				}
				else{
					QString fullPath = clientRootPath; fullPath += "\\"; fullPath += deletedFiles[i].substr(1).c_str();
					deleteDirectory(fullPath.toStdWString());
				}
			}
			print("");
		}

		if (newFilesStr != "") {
			vector<string> newFiles = split(newFilesStr, '\n');
			print("Dodano pliki/foldery:");
			for (int i = 0; i < newFiles.size(); i++) {
				print(newFiles[i].substr(1).c_str());

				if (newFiles[i][0] == 'f') {

					// f znaczy �e mamy do czynienia z PLIKIEM (nie folderem)
					// wczytujemy nazw� pliku z rozszerzeniem

					QString fullPath = clientRootPath; fullPath += "\\"; fullPath += newFiles[i].substr(1).c_str();
					mkFile(fullPath.toStdString().c_str());

				}
				else{
					QString fullPath = clientRootPath; fullPath += "\\"; fullPath += newFiles[i].substr(1).c_str();
					createDir(fullPath.toStdWString().c_str());
				}
			}
			print("");
		}

	}




}

void _readFromClient(SOCKET s, char* data, int ndata) {
	if (data[0] == 'm') {
		getDB()->print("Text message from some client:");
		getDB()->print(&data[1]);
	}
	else if (data[0] == 'r'){
		// here is receive confirmation from client -> serv resuming sending messages to clients
		servIsWaitingForRcvConfirmation = false;
	}

}

QStringList normalizePathList(QStringList dirs, QStringList files, string rootPath) {
	QStringList allFilesAndDirs = QStringList(_allDirsAndSubdirsList);

	int size = allFilesAndDirs.size(); string t;

	for (int i = 0; i < size; i++) {
		t = "d" + getSubPathOfFile(allFilesAndDirs[i].toStdString().c_str(), rootPath);
		allFilesAndDirs[i] = t.c_str();
	}

	allFilesAndDirs.append(_allFilesList);

	int i = size; size = allFilesAndDirs.size();
	for (; i < size; i++) {
		t = "f" + getSubPathOfFile(allFilesAndDirs[i].toStdString().c_str(), rootPath);
		allFilesAndDirs[i] = t.c_str();
	}

	return allFilesAndDirs;
}

void _newClient(SOCKET s) {
	getDB()->print("New client connected!");
	getDB()->print("Start init-client sync process of all files");

	// Pobieramy list� plik�w i folder�w z root-path
	// Nie b�dzie kaza� klientowi usuwa� �adnych konkretnych plik�w dlatego lista deletedFiles (emptyList) jest list� PUST�
	getAllStuffFromDir(servRootPath.toStdString().c_str());

	QStringList allFilesAndDirs = normalizePathList(_allDirsAndSubdirsList, _allFilesList, servRootPath.toStdString());
	QStringList emptyList; emptyList.clear();

	// Wysy�amy komunikat o startowej synchronizacji plik� dla nowego klienta
	// i - oznacza "init-sync" informuj�c klienta �e otrzymany komunikat b�dzie zawiera� info o startowej synchronizacji
	getDB()->sync_step2(allFilesAndDirs, emptyList, s, L"i");
}

void DropBasket::startServer() {

	servRootPath = ui.fServRootPath->text();

	getPathState();
	oldDirsState = QStringList(currDirsState);
	oldFilesState = QStringList(currFilesState);

	fileWatcher = new QFileSystemWatcher(this);
	fileWatcher->addPath(servRootPath);


	getAllStuffFromDir(servRootPath.toStdString().c_str());

	for (int i = 0; i < _allFilesList.size(); i++) {
		print(_allFilesList[i]);
		fileWatcher->addPath(_allFilesList[i]);
	}

	for (int i = 0; i < _allDirsAndSubdirsList.size(); i++) {
		print(_allDirsAndSubdirsList[i]);
		fileWatcher->addPath(_allDirsAndSubdirsList[i]);
	}

	connect(fileWatcher, SIGNAL(directoryChanged(const QString&)), this, SLOT(pathChangeDetect(const QString&)));
	connect(fileWatcher, SIGNAL(fileChanged(const QString&)), this, SLOT(fileChangeDetect(const QString&)));





	ui.fJoinToServ->setEnabled(false);
	ui.fHostAddrToConnect->setEnabled(false);
	ui.fHostPortToConnect->setEnabled(false);
	ui.fClientRootPath->setEnabled(false);
	// ...
	ui.fStartServ->setEnabled(false);
	ui.fHostAddr->setEnabled(false);
	ui.fHostPort->setEnabled(false);
	ui.fServRootPath->setEnabled(false);
	// ...
	//ui.fServMsg->setEnabled(true);
	//ui.fServSend->setEnabled(true);

	TCPSERV_readFromClient = _readFromClient;
	TCPSERV_newClient = _newClient;
	::startServer(ui.fHostPort->text().toStdString().c_str());
}

void DropBasket::pathChangeDetect(const QString& dir) {
	print("Detect change in root path or sub path! Wait 4sec and sync!");
	Sleep(4000);
	synchronize();
}

bool fileExists(QString path) {
	QFileInfo checkFile(path);

	if (checkFile.exists() && checkFile.isFile()) {
		return true;
	}
	else {
		return false;
	}
}

void DropBasket::fileChangeDetect(const QString& dir) {
	if (!fileExists(dir)) {
		return;
	}

	print("Detect change in file! Wait 4sec and sync!");
	Sleep(4000);
	for (int i = 0; i < TCPSERV_clients.size(); i++) {
		sendBinary(TCPSERV_clients[i], dir.toStdString().c_str());
	}
}

void DropBasket::clientSend() {
	//clientSock->write("m" + ui.fClientMsg->text().toUtf8());
	print(clientSock->errorString());
	print("State: ", (int)clientSock->state());
}

void DropBasket::servSend() {
	QString msg = "m" + ui.fServMsg->text();
	for (int i = 0; i < TCPSERV_clients.size(); i++) {
		sendToClient(TCPSERV_clients[i], (char*)msg.toStdString().c_str(), msg.size());
		//clientsConnected[i]->sock->write(msg.toUtf8());
	}
}

char* encodeLong(long value) {
	char* out = new char[4];

	for (int i = 0; i < sizeof(long); i++) {
		out[i] = (char)((value >> (8 * i)) & 0xff);
	}

	return out;
}

char* getFileData(const char* filepath, long &entire_size) {

	//===========================================================================================
	//--> Funkcja wczytuje zawarto�� pliku binarnego oraz jego rozmiar
	//--> Nast�pnia skleja rozmiar + zawarto��
	//--> I to wszystko zwraca w formie zmiennej string
	//===========================================================================================

	string subPathOfFile_str = getSubPathOfFile(filepath, servRootPath.toStdString());
	char* subPathOfFile = (char*)subPathOfFile_str.c_str();
	long lenOfSubPath = subPathOfFile_str.size();
	char* encodedLenOfSubPath = encodeLong(lenOfSubPath);

	long filesize = -1;
	char* content = readBinary(filepath, filesize);
	char ch;

	// print("filesize = ", filesize);

	char* encodedFileSize = encodeLong(filesize);
	
	entire_size = 1 + 4 + lenOfSubPath + 4 + filesize;

	char *fileData = new char[entire_size];

	fileData[0] = 'b';

	int i = 1;
	memcpy(&fileData[i], encodedLenOfSubPath, 4); i += 4;
	memcpy(&fileData[i], subPathOfFile, lenOfSubPath); i += lenOfSubPath;
	memcpy(&fileData[i], encodedFileSize, 4); i += 4;
	memcpy(&fileData[i], content, filesize); i += filesize;

	return fileData;
}


void DropBasket::sendBinary(SOCKET client, const char* fullFilePath) {
	long size; char* data;

	data = getFileData(fullFilePath, size);

	sendToClient(client, data, size);

	print("\nContent of file:");
	print(fullFilePath);
	print("sent successfully!\n");
}


// lista plik�w (pe�na �ciezka do pliku) kt�re trzeba przes�a� klientowi/klientom
vector<string> binaryFilesToSend;

void DropBasket::sendAllBinaryFiles(SOCKET client) {
	for (int i = 0; i < binaryFilesToSend.size(); i++) {
		sendBinary(client, binaryFilesToSend[i].c_str());
	}
}


void DropBasket::synchronize() {
	QStringList newFilesAndDirs, deletedFilesAndDirs;
	sync_step1(newFilesAndDirs, deletedFilesAndDirs);
	sync_step2(newFilesAndDirs, deletedFilesAndDirs);

	// Add notify watcher to new folders and files
	QString t;
	for (int i = 0; i < newFilesAndDirs.size(); i++) {
		t = servRootPath + "\\" + newFilesAndDirs[i].mid(1);
		print(t);
		fileWatcher->addPath(t);
	}
}

void DropBasket::sync_step1(QStringList &newFilesAndDirs, QStringList &deletedFilesAndDirs) {
	print("BEGIN SYNC");
	print("\tstart sending changes to connected clients");

	// get current path state from root path
	getPathState();

	// detect differences from old path state and current path state
	checkPathDiff(newFilesAndDirs, deletedFilesAndDirs);
}

void DropBasket::sync_step2(QStringList &newFilesAndDirs, QStringList &deletedFilesAndDirs, SOCKET concreteClient, wchar_t* messageID) {

	// if no changes then exit
	if (newFilesAndDirs.size() == 0 && deletedFilesAndDirs.size() == 0) {
		print("No changes detected in root path!");
		return;
	}


	binaryFilesToSend.clear();
	wstring strToSend = L"";
	
	for (int i = 0; i < newFilesAndDirs.size(); i++){
		strToSend += newFilesAndDirs[i].toStdWString() + L"\n";
		if (newFilesAndDirs[i][0] == 'f') {
			print("\nContent of file:");
			string _filename = "";
			wstring fullPath = servRootPath.toStdWString(); fullPath += L"\\"; fullPath += newFilesAndDirs[i].toStdWString().substr(1);
			WStringToString(_filename, fullPath);
			print(_filename.c_str());
			print("buffored successfully\n");
			binaryFilesToSend.push_back(_filename);
		}
	}

	strToSend += L":";
	for (int i = 0; i < deletedFilesAndDirs.size(); i++){
		strToSend += deletedFilesAndDirs[i].toStdWString() + L"\n";
	}


	strToSend = messageID + strToSend;
	int msgSize = strToSend.size() * sizeof(wchar_t);
	char *dest = new char[msgSize];
	wcstombs(dest, strToSend.c_str(), msgSize);

	char t[255];
	char *copy_of_s_msg;

	if (messageID == L"s") {
		for (int i = 0; i < TCPSERV_clients.size(); i++) {
			copy_of_s_msg = new char[msgSize];
			strcpy(copy_of_s_msg, dest);
			sendToClient(TCPSERV_clients[i], copy_of_s_msg, msgSize);
			sendAllBinaryFiles(TCPSERV_clients[i]);
			sprintf(t, "\tSending changes to client %d successfully!", i);
			print(t);
		}
		oldDirsState = QStringList(currDirsState);
		oldFilesState = QStringList(currFilesState);
	}
	else if (messageID == L"i") {
		sendToClient(concreteClient, dest, msgSize);
		sendAllBinaryFiles(concreteClient);
		sprintf(t, "\tSending sync-init info to new_client successfully!");
		print(t);
	}

	print("END SYNC\n");
}


DropBasket::DropBasket(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	// connect SIGNALs with SLOTs (attach ui events to functions)
	QObject::connect(ui.fStartServ, SIGNAL(clicked()), this, SLOT(startServer()));
	QObject::connect(ui.fJoinToServ, SIGNAL(clicked()), this, SLOT(startClient()));
	QObject::connect(ui.fClientSend, SIGNAL(clicked()), this, SLOT(clientSend()));
	QObject::connect(ui.fServSend, SIGNAL(clicked()), this, SLOT(servSend()));
	QObject::connect(dbwin, SIGNAL(closedSignal()), this, SLOT(closeApp()));

	// show IP address
	ui.fHostAddr->setText(getIPaddress());
}

DropBasket::~DropBasket()
{

}

void DropBasket::closeEvent(QCloseEvent *event)
{
	QMessageBox::StandardButton resBtn = QMessageBox::question(this, "DropBasket",
		tr("Are you sure?\n"),
		QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
		QMessageBox::Yes);
	if (resBtn != QMessageBox::Yes) {
		event->ignore();
	}
	else {
		stop_all = true;
		destroy_all();

		event->accept();
	}

}

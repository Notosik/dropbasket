#include "TcpServ.h"

QApplication *a;

extern bool stop_all = false;

vector<SOCKET> sendingBuffer_clients;
vector<char*> sendingBuffer_data;
vector<long> sendingBuffer_datacount;

vector<SOCKET> TCPSERV_clients;
void(*TCPSERV_readFromClient)(SOCKET, char*, int);
void(*TCPSERV_newClient)(SOCKET);
bool servIsWaitingForRcvConfirmation;

int poll_fd_count = 1;
int new_clients = 0;
pollfd pollfd[10];

void sendToClient(SOCKET client, char* data, long datacount) {

	// Do ka�dych wysy�anych danych dodajemy na pocz�tku informacje o wielko�ci tych danych
	// Taka informacja jest zakodowana ZAWSZE na czterech bajtach (4xchar)

	char* msgSizeCoded = encodeLong(datacount + 4);

	char *out_data = new char[datacount + 4];
	memcpy(out_data, msgSizeCoded, 4);
	memcpy(&out_data[4], data, datacount);
	delete data;

	sendingBuffer_clients.push_back(client);
	sendingBuffer_data.push_back(out_data);
	sendingBuffer_datacount.push_back(datacount + 4);
}

void startServer(const char* addrPort) {

	WSADATA wsaData;
	int iResult;

	SOCKET ServSocket = INVALID_SOCKET;
	//SOCKET ClientSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char* recvbuf = new char[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		getDB()->print("WSAStartup failed with error: %d\n", iResult);
		return;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, addrPort, &hints, &result);
	if (iResult != 0) {
		getDB()->print("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return;
	}

	//sockaddr_in* addr = (sockaddr_in*)(result->ai_addr);
	//if (addr->sin_addr.s_addr > 0)
		//getDB()->print("Serv IP address coded in LONG is: ", (int)addr->sin_addr.s_addr); //  .s_addr = 1840107721;
	//inet_aton
	//addr->sin_port = addrPort;
	//result->ai_addr = (sockaddr*)addr;

	// Create a SOCKET for connecting to server
	ServSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ServSocket == INVALID_SOCKET) {
		getDB()->print("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return;
	}

	// Setup the TCP listening socket
	iResult = bind(ServSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		getDB()->print("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ServSocket);
		WSACleanup();
		return;
	}

	freeaddrinfo(result);

	iResult = listen(ServSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		getDB()->print("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ServSocket);
		WSACleanup();
		return;
	}






	//====================================================================

	poll_fd_count = 1;
	new_clients = 0;

	pollfd[0].fd = ServSocket;
	pollfd[0].events = POLLIN;

	for (int i = 1; i < 10; i++) {
		pollfd[i].fd = 0;
		pollfd[i].events = 0;
	}

	while (!stop_all) {

		a->processEvents();

		poll_fd_count += new_clients;
		new_clients = 0;

		while (sendingBuffer_clients.size() > 0 && !servIsWaitingForRcvConfirmation) {

			//===============================================================
			//--> IF WE HAVE SOME SEND REQUEST IN BUFFER
			//===============================================================

			iSendResult = send(sendingBuffer_clients.front(), sendingBuffer_data.front(), sendingBuffer_datacount.front(), 0);
			if (iSendResult == SOCKET_ERROR) {
				printf("send failed with error: %d\n", WSAGetLastError());
				closesocket(pollfd[poll_fd_count].fd);
				WSACleanup();
				return;
			}

			getDB()->print("Server sending machine sent:");
			getDB()->print(sendingBuffer_data.front());

			sendingBuffer_clients.erase(sendingBuffer_clients.begin());
			sendingBuffer_data.erase(sendingBuffer_data.begin());
			sendingBuffer_datacount.erase(sendingBuffer_datacount.begin());

			servIsWaitingForRcvConfirmation = true;
		}

		WSAPoll(pollfd, poll_fd_count, 1);

		

		for (int fd_index = 0; fd_index < poll_fd_count; fd_index++) {
			if (pollfd[fd_index].revents & POLLIN) {
				getDB()->print("WSPAPoll breaked");
				if (pollfd[fd_index].fd == ServSocket) {

					//===============================================================
					//--> NEW CLIENT FOUND
					//===============================================================

					TCPSERV_clients.push_back(accept(ServSocket, NULL, NULL));
					if (TCPSERV_clients.back() == INVALID_SOCKET) {
						getDB()->print("accept failed with error: %d\n", WSAGetLastError());
						closesocket(ServSocket);
						WSACleanup();
						return;
					}

					pollfd[poll_fd_count].fd = TCPSERV_clients.back();
					pollfd[poll_fd_count].events = POLLIN;

					//if (TCPSERV_clients.size() == 1)
					//	int test = 0;

					new_clients++;

					if (TCPSERV_newClient != nullptr)
						TCPSERV_newClient(pollfd[poll_fd_count].fd);

					

					//break;
					//update_maxfd(client_sockfd, &max_fd);
					//printf("Adding client on fd %d\n", client_sockfd);

				}
				else {

					//===============================================================
					//--> READ DATA FROM CLIENT
					//===============================================================

					iResult = recv(pollfd[fd_index].fd, recvbuf, recvbuflen, 0);
					if (iResult > 0) {

						//===============================================================
						//--> READ (COUSE DATA LEN IS GREATER THEN ZERO)
						//===============================================================

						recvbuf[iResult] = '\0';

						getDB()->print("Read data from client:");
						getDB()->print(recvbuf);

						if (TCPSERV_readFromClient != nullptr)
							TCPSERV_readFromClient(pollfd[fd_index].fd, recvbuf, iResult);

						// Echo the buffer back to the sender
						/*iSendResult = send(pollfd[fd_index].fd, recvbuf, iResult, 0);
						if (iSendResult == SOCKET_ERROR) {
							printf("send failed with error: %d\n", WSAGetLastError());
							closesocket(pollfd[fd_index].fd);
							WSACleanup();
							return;
						}*/
						//printf("Bytes sent: %d\n", iSendResult);
					}
					else if (iResult == 0)

						//===============================================================
						//--> CLIENT CLOSE CONNECTION (COUSE SEND 0 BYTES)
						//===============================================================

						printf("Connection closing...\n");
					else  {

						//===============================================================
						//--> ERROR WHILE RECEIVING DATA FROM CLIENT
						//===============================================================

						printf("recv failed with error: %d\n", WSAGetLastError());
						closesocket(pollfd[fd_index].fd);
						WSACleanup();
						return;
					}

				}
			}
		}
	}
	//===============================================================
	//--> CLOSE ALL CLIENTS
	//===============================================================

	destroy_all();

}

void destroy_all() {
	for (int i = 0; i < poll_fd_count; i++) {
		// shutdown the connection since we're done
		int iResult = shutdown(pollfd[i].fd, SD_SEND);
		if (iResult == SOCKET_ERROR) {
			printf("shutdown failed with error: %d\n", WSAGetLastError());
			closesocket(pollfd[i].fd);
			WSACleanup();
			return;
		}


		// cleanup
		closesocket(pollfd[i].fd);
		WSACleanup();
	}
}
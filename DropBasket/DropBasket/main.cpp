#include "dropbasket.h"
#include <QtWidgets/QApplication>

DropBasket* dbwin;

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	dbwin = new DropBasket();
	setDB(dbwin);

	dbwin->show();
	return a.exec();
}

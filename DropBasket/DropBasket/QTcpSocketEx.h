#pragma once

#include <QtNetwork>
#include <QtWidgets/QMainWindow>
#include <QWidget>
#include <QObject>

class QTcpSocketEx : public QObject {
	Q_OBJECT
		QTcpSocket *Socket;
public:
	QTcpSocket* sock;
	QTcpSocketEx(QTcpSocket* sock);

signals:
	void readyReadEx(QTcpSocket* sender);

public slots:
	void _readyRead();


};